import React from 'react';
//Libraries
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
//Components and Helpers
import Servicio from "./components/Servicio";
import FormServicio from "./components/Servicio/servicio";
import Stock from "./components/Stock";
import Item from "./components/Stock/formulario";
import NoMatch from "./components/NoMatch";

function App() {
  return (
      <div>
          <section className="section">
              <Router>
                  <Switch>
                      <Route exact path="/" component={Stock}/>
                      <Route exact path="/servicio" component={FormServicio}/>
                      <Route exact path="/obra/:id" component={Servicio}/>
                      <Route exact path="/stock" component={Item}/>
                      <Route component={NoMatch} />
                  </Switch>
              </Router>
          </section>
          <ToastContainer autoClose={2000} newestOnTop pauseOnVisibilityChange/>
      </div>
  );
}

export default App;
