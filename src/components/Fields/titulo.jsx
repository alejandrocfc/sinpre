import React from "react";
import {renderSimpleItem} from "../../helpers/utils";

export default function Titulo({...props}) {
    const {data,idx,onChange} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label className="label">{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Nombre</label>
                            <input className="input" type="text" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)} required/>
                        </div>
                        <div className="field">
                            <label htmlFor="subtype" className="label">Tipo</label>
                            <select className="select is-fullwidth" id="subtype" name="subtype" value={data.subtype} onChange={e=>onChange(e,idx)}>
                                <option value="h1">Titulo</option>
                                <option value="h2">SubTitulo</option>
                            </select>
                        </div>
                    </div>:
                    <div dangerouslySetInnerHTML={{__html:renderSimpleItem(data)}}/>
            }
        </React.Fragment>
    )
}
