import React from "react";

export default function Dimension({...props}) {
    const {data,idx,onChange} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label>{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Nombre</label>
                            <input type="text" className="input" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="field">
                            <label className="check-label label pt1">Obligatorio
                                <input type="checkbox" id="required" name="required" checked={data.required} onChange={e=>onChange(e,idx)}/>
                                <span className="checkmark"/>
                            </label>
                        </div>
                        <div className="field">
                            <label htmlFor="placeholder" className="label">Texto de ayuda</label>
                            <input type="text" className="input" id="placeholder" name="placeholder" value={data.placeholder} onChange={e=>onChange(e,idx)}/>
                        </div>
                    </div>:
                    <React.Fragment>
                        <h3 className="subtitle">{data.label?data.label:"Dimensión"}</h3>
                        <p className="label">{data.placeholder}</p>
                        <div className="columns">
                            <div className="field column">
                                <label htmlFor="lat" className="label">Ancho</label>
                                {data.required && <sup><abbr title="required">*</abbr></sup>}
                                <input type="text" className="input" id="lat" name="lat"/>
                            </div>
                            <div className="field column">
                                <label htmlFor="long" className="label">Alto</label>
                                {data.required && <sup><abbr title="required">*</abbr></sup>}
                                <input type="text" className="input" id="long" name="long"/>
                            </div>
                            <div className="field column">
                                <label htmlFor="alt" className="label">Largo</label>
                                {data.required && <sup><abbr title="required">*</abbr></sup>}
                                <input type="text" className="input" id="alt" name="alt"/>
                            </div>
                        </div>
                    </React.Fragment>
            }
        </React.Fragment>
    )
}
