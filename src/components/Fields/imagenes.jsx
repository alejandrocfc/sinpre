import React from "react";

export default function Imagenes({...props}) {
    const {data,idx,onChange} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label className="label">{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Nombre</label>
                            <input type="text" className="input" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="columns">
                            <div className="field column">
                                <label className="check-label label pt1">Obligatorio
                                    <input type="checkbox" id="required" name="required" checked={data.required} onChange={e=>onChange(e,idx)}/>
                                    <span className="checkmark"/>
                                </label>
                            </div>
                            <div className="field column">
                                <label className="check-label label pt1">Multiples
                                    <input type="checkbox" id="multi" name="multi" checked={data.multi} onChange={e=>onChange(e,idx)}/>
                                    <span className="checkmark"/>
                                </label>
                            </div>
                        </div>
                        {
                            data.multi && <div className="field">
                                <label htmlFor="cant" className="label">Cantidad</label>
                                <input className="input" type="number" id="cant" name="cant" value={data.cant} onChange={e=>onChange(e,idx)}/>
                            </div>
                        }
                        <div className="field">
                            <label htmlFor="placeholder" className="label">Texto de ayuda</label>
                            <input type="text" className="input" id="placeholder" name="placeholder" value={data.placeholder} onChange={e=>onChange(e,idx)}/>
                        </div>
                    </div>:
                    <React.Fragment>
                        <h3 className="subtitle">{data.label?data.label:data.name}</h3>
                        <p>{data.placeholder}</p>
                        <p>Múltiples: {data.multi?"Si":"No"}</p>
                        {data.multi&&<p>Cantidad: {data.cant}</p>}
                    </React.Fragment>
            }
        </React.Fragment>
    )
}
