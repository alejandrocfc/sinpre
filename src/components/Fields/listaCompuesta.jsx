import React from "react";

export default function ListaCompuesta({...props}) {
    const {data,idx,onChange,stocks} = props;
    const form = stocks.find(({id_stock})=>id_stock == data.form);
    return (
        <React.Fragment>
        {
            data.edit ?
                <div>
                    <label className="label">{data.name}</label>
                    <div className="field">
                        <label htmlFor="label" className="label">Nombre</label>
                        <input type="text" className="input" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)}/>
                    </div>
                    <div className="field">
                        <label className="check-label label">Obligatorio
                            <input type="checkbox" id="required" name="required" checked={data.required} onChange={e=>onChange(e,idx)}/>
                            <span className="checkmark"/>
                        </label>
                    </div>
                    <div className="field">
                        <label htmlFor="placeholder" className="label">Texto de ayuda</label>
                        <input type="text" className="input" id="placeholder" name="placeholder" value={data.placeholder} onChange={e=>onChange(e,idx)}/>
                    </div>
                    <div className="columns">
                        <div className="column field">
                            <label htmlFor="number" className="label">Cantidad de opciones</label>
                            <input className="input" type="number" id="number" name="number" min={0} value={data.number} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="column">
                            <div className="field" style={{width:"100%",position:"relative",top:"20%"}}>
                                <label className="check-label label">Permitir agregar más opciones desde la app
                                    <input type="checkbox" name="variable" checked={data.variable} onChange={e=>onChange(e,idx)}/>
                                    <span className="checkmark"/>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <label htmlFor="form" className="label">SubFormulario:</label>
                        <select id="subtype" className="select is-fullwidth" name="form" value={data.form} onChange={e=>onChange(e,idx)}>
                            <option value="">Seleccione...</option>
                            {
                                stocks.map(({id_stock,nombre_stock})=><option key={id_stock} value={id_stock}>{nombre_stock}</option>)
                            }
                        </select>
                    </div>
                </div>:
                <React.Fragment>
                    <h2>Lista compuesta</h2>
                    <p>Cantidad de opciones: {data.number}</p>
                    <p>Variable: {data.variable?"SI":"NO"}</p>
                    <p>Formulario: {form?form.nombre_stock:""}</p>
                </React.Fragment>
        }
    </React.Fragment>
    )
}
