import React from "react";

export default function Field({...props}) {
    const{idx,deleteItem,editItem} = props;
    return (
        <React.Fragment>
            <div className="actions">
                <button className="button" onClick={e=>deleteItem(idx)}>
                    <span><i className='fas fa-trash'></i></span>
                </button>
                <button className="button" onClick={e=>editItem(idx)}>
                    <span><i className='fas fa-edit'></i></span>
                </button>
            </div>
            {props.children}
        </React.Fragment>
    )
}
