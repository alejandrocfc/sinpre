import React from "react";
import { renderTextArea} from "../../helpers/utils";

export default function AreaTexto({...props}) {
    const {data,idx,onChange} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label className="label">{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Nombre</label>
                            <input type="text" className="input" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="field">
                            <label className="check-label label pt1">Obligatorio
                                <input type="checkbox" id="required" name="required" checked={data.required} onChange={e=>onChange(e,idx)}/>
                                <span className="checkmark"/>
                            </label>
                        </div>
                        <div className="field">
                            <label htmlFor="placeholder" className="label">Texto de ayuda</label>
                            <input type="text" className="input" id="placeholder" name="placeholder" value={data.placeholder} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="field">
                            <label htmlFor="maxlength" className="label">Cantidad máxima de caracteres</label>
                            <input type="number" className="input" id="maxlength" name="maxlength" value={data.maxlength} onChange={e=>onChange(e,idx)}/>
                        </div>
                    </div>:
                    <div dangerouslySetInnerHTML={{__html:renderTextArea(data)}}/>
            }
        </React.Fragment>
    )
}
