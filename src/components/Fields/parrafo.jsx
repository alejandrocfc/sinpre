import React from "react";
import {renderSimpleItem} from "../../helpers/utils";

export default function Parrafo({...props}) {
    const {data,idx,onChange} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label className="label">{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Contenido</label>
                            <input className="input" type="text" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)} required/>
                        </div>
                    </div>:
                    <div className="f5" dangerouslySetInnerHTML={{__html:renderSimpleItem(data)}}/>
            }
        </React.Fragment>
    )
}
