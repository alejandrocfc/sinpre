import React from "react";
import { renderNumber} from "../../helpers/utils";

export default function CampoNumerico({...props}) {
    const {data,idx,onChange} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label className="label">{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Nombre</label>
                            <input className="input" type="text" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="field">
                            <label className="check-label label pt1">Obligatorio
                                <input type="checkbox" id="required" name="required" checked={data.required} onChange={e=>onChange(e,idx)}/>
                                <span className="checkmark"/>
                            </label>
                        </div>
                        <div className="columns">
                            <div className="field column">
                                <label htmlFor="min" className="label">Valor mínimo</label>
                                <input className="input" type="number" id="min" name="min" value={data.min} onChange={e=>onChange(e,idx)}/>
                            </div>
                            <div className="field column">
                                <label htmlFor="max" className="label">Valor máximo</label>
                                <input className="input" type="number" id="max" name="max" value={data.max} onChange={e=>onChange(e,idx)}/>
                            </div>
                            <div className="field column">
                                <label className="check-label label pt1">Permitir decimales
                                    <input type="checkbox" id="decimals" name="decimals" checked={data.decimals} onChange={e=>onChange(e,idx)}/>
                                    <span className="checkmark"/>
                                </label>
                            </div>
                        </div>
                    </div>:
                    <div dangerouslySetInnerHTML={{__html:renderNumber(data)}}/>
            }
        </React.Fragment>
    )
}
