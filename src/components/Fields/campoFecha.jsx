import React from "react";
import {renderInput} from "../../helpers/utils";

export default function CampoFecha({...props}) {
    const {data,idx,onChange} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label className="label">{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Nombre</label>
                            <input className="input" type="text" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="field">
                            <label className="check-label label pt1">Obligatorio
                                <input type="checkbox" id="required" name="required" checked={data.required} onChange={e=>onChange(e,idx)}/>
                                <span className="checkmark"/>
                            </label>
                        </div>
                        <div className="field">
                            <label htmlFor="subtype" className="label">Tipo</label>
                            <select className="select is-fullwidth" id="subtype" name="subtype" value={data.subtype} onChange={e=>onChange(e,idx)}>
                                <option value="date">Fecha DD/MM/YYYY</option>
                                <option value="time">Hora HH:mm AM/PM</option>
                                <option value="datetime-local">Fecha y Hora DD/MM/YYYY HH:mm AM/PM</option>
                            </select>
                        </div>
                    </div>:
                    <div dangerouslySetInnerHTML={{__html:renderInput(data)}}/>
            }
        </React.Fragment>
    )
}
