import React from "react";
import { renderSelect} from "../../helpers/utils";

export default function ListaOpciones({...props}) {
    const {data,idx,onChange,addOption,onChangeOption,removeOption} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label className="label">{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Nombre</label>
                            <input className="input" type="text" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="field">
                            <label className="check-label label pt1">Obligatorio
                                <input type="checkbox" id="required" name="required" checked={data.required} onChange={e=>onChange(e,idx)}/>
                                <span className="checkmark"/>
                            </label>
                        </div>
                        <div className="field">
                            <label htmlFor="placeholder" className="label">Texto de ayuda</label>
                            <input className="input" type="text" id="placeholder" name="placeholder" value={data.placeholder} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="field">
                            <label htmlFor="maxlength" className="label">Opciones:</label>
                            <table className="table is-fullwidth">
                                <thead>
                                    <tr>
                                        <th style={{width:"50%"}}>Nombre</th>
                                        <th style={{width:"40%"}}>Valor</th>
                                        <th style={{width:"10%"}}/>
                                    </tr>
                                </thead>
                                <tbody>
                                {
                                    data.options.map( (opt,key)=>(
                                        <tr key={key}>
                                            <td>
                                                <input className="input" type="text" id="label" name="label" value={opt.label} onChange={e=>onChangeOption(e,idx,key)}/>
                                            </td>
                                            <td>
                                                <input className="input" type="text" id="label" name="value" value={opt.value} onChange={e=>onChangeOption(e,idx,key)}/>
                                            </td>
                                            <td className="has-text-centered">
                                                <button className="button is-danger tiny" onClick={e=>removeOption(idx,key)}><i className="fas fa-times"/></button>
                                            </td>
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                            <button className="button is-light" onClick={e=>addOption(idx)}>Agregar opción</button>
                        </div>
                    </div>:
                    <div dangerouslySetInnerHTML={{__html:renderSelect(data)}}/>
            }
        </React.Fragment>
    )
}
