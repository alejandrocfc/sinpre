import React from "react";
import {renderInput} from "../../helpers/utils";

export default function CampoTexto({...props}) {
    const {data,idx,onChange} = props;
    return (
        <React.Fragment>
            {
                data.edit ?
                    <div>
                        <label className="label">{data.name}</label>
                        <div className="field">
                            <label htmlFor="label" className="label">Nombre</label>
                            <input className="input" type="text" id="label" name="label" value={data.label} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="field">
                            <label className="check-label label pt1">Obligatorio
                                <input type="checkbox" id="required" name="required" checked={data.required} onChange={e=>onChange(e,idx)}/>
                                <span className="checkmark"/>
                            </label>
                        </div>
                        <div className="field">
                            <label htmlFor="placeholder" className="label">Texto de ayuda</label>
                            <input className="input" type="text" id="placeholder" name="placeholder" value={data.placeholder} onChange={e=>onChange(e,idx)}/>
                        </div>
                        <div className="columns">
                            <div className="field column">
                                <label htmlFor="maxlength" className="label">Longitud máxima</label>
                                <input className="input" type="number" id="maxlength" name="maxlength" value={data.maxlength} onChange={e=>onChange(e,idx)}/>
                            </div>
                            <div className="field column">
                                <label htmlFor="subtype" className="label">Tipo</label>
                                <select className="select is-fullwidth" id="subtype" name="subtype" value={data.subtype} onChange={e=>onChange(e,idx)}>
                                    <option value="text">Texto</option>
                                    <option value="email">Correo</option>
                                    <option value="tel">Teléfono</option>
                                    <option value="color">Color</option>
                                    <option value="url">URL</option>
                                </select>
                            </div>
                        </div>
                    </div>:
                    <div dangerouslySetInnerHTML={{__html:renderInput(data)}}/>
            }
        </React.Fragment>
    )
}
