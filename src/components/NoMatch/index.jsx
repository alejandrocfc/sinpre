import React from "react";

export default function NoMatch({...props}) {
    const message = (props.location.state && props.location.state.message) ? props.location.state.message:"Página no encontrada";
    return (
        <div>
            <div className="fl w-50">
                <h1>ERROR</h1>
                <p>{message}</p>
            </div>
            <div className="fr w-50">
                <figure>
                    <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 489.9 201.31"><defs>
                        <style></style></defs><title>404</title>
                        <path className="cls-1" d="M326.9,132.2C269.7,84.5,203,32.1,88.6,55.9c-33.7,15.2-38.8,49.7-21.4,78.7,18.2,30.3,59.6,54.8,59.6,54.8H38.6S10,187,10,220.4,33.8,249,33.8,249H210.1s28.6,4.8,28.6-28.6-61.9-71.5-61.9-71.5,50.8,13.4,75.4,18.6" transform="translate(-10 -49.97)" style={{fill:'#999'}}/>
                        <path id="arm" className="cls-2" d="M367.4,179.8a16.58,16.58,0,0,1-16.6-16.7,16.35,16.35,0,0,1,5.3-12.1l76.2-71.5a16.69,16.69,0,1,1,23.6,23.6,5.72,5.72,0,0,1-.8.7l-76.2,71.5A17.16,17.16,0,0,1,367.4,179.8Z" transform="translate(-10 -49.97)" style={{fill:'#999'}}/>
                        <circle className="cls-2" cx="335.9" cy="139.43" r="57.2" style={{fill:'#999'}}/>
                        <path className="cls-2" d="M412.6,248.9H293.5a16.74,16.74,0,0,1-15.7-11L239.7,133.1a16.7,16.7,0,0,1,31.4-11.4l34.1,93.8H412.6a16.7,16.7,0,0,1,0,33.4Z" transform="translate(-10 -49.97)" style={{fill:'#999'}}/>
                        <g id="gotas"><path id="g1" className="cls-3" d="M421.65,173.22s14.3-42.9,28.6-23.8S421.65,173.22,421.65,173.22Z" transform="translate(-10 -49.97)" style={{fill:"#"}}/>
                            <path id="g2" className="cls-3" d="M437.25,216.12s31.1-11.2,26.6,5.7S437.25,216.12,437.25,216.12Z" transform="translate(-10 -49.97)" style={{fill:"#"}}/>
                            <path id="g3" className="cls-3" d="M445.45,187.52s52.4-31.2,54.4,2.3S445.45,187.52,445.45,187.52Z" transform="translate(-10 -49.97)" style={{fill:"#"}}/></g>
                    </svg>
                </figure>
            </div>
        </div>
    )
}
