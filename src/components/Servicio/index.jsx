import React, {Component} from "react";
import { withRouter } from "react-router-dom";
//Libraries
import {Link} from "react-router-dom";
import Moment from "moment";
import {ObrasApi} from "../../helpers/request";

class Servicio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            obra:{},
            loading:true,
            hasForm:false
        }
    }

    async componentDidMount() {
        try{
            const { id } = this.props.match.params;
            const obra = await ObrasApi.getInfo(id);
            if(obra.data.length > 0){
                const formulario = await ObrasApi.getForm(obra.data[0].id_solicitud);
                this.setState({obra:obra.data[0],loading:false,hasForm:!!formulario.data.id_formulario})
            }else{
                this.props.history.push({
                    pathname:"/no-match",
                    state:{message:"Obra no encontrada: "+obra.msj}
                })
            }
        }catch (e) {
            console.log(e);
        }
    }

    render() {
        if(this.state.loading) return <p>Cargando...</p>;
        const { obra } = this.state;
        return (
            <div className="container">
                <article className="box">
                    <div className="ph3">
                        <h2 className="subtitle">{obra.nombre_solicitud}</h2>
                        <div className="description">
                            <p>Descripción: {obra.descripcion_solicitud}</p>
                            <p>Torre: {obra.nombre_torre} {obra.number_torre}</p>
                            <p>Latitud: {obra.torre_latitud}, Longitud: {obra.torre_longitud}</p>
                            <p>Anchor: {obra.torre_anchor_id}</p>
                            <p>Ticket: {obra.ticket_solicitud}</p>
                            <p>Fecha asignación: {obra.fecha_asignacion}</p>
                            <p>Fecha ejecución: {obra.fecha_ejecucion}</p>
                        </div>
                        <hr className="horizontal"/>
                        <Link to={{
                            pathname:"/servicio",
                            state:{obra:this.state.obra}
                        }}>
                            <button className="button blue is-small">{this.state.hasForm ? "Editar" : "Agregar"} Formulario</button>
                        </Link>
                    </div>
                </article>
                <table className="table is-fullwidth">
                    <thead>
                        <tr>
                            <th>Creado Por</th>
                            <th>Fecha Creación</th>
                            <th>Última Actualización</th>
                            <th>Detalle</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Usuario</td>
                            <td>{Moment().format('DD/MM/YYYY HH:mm')}</td>
                            <td>{Moment().format('DD/MM/YYYY HH:mm')}</td>
                            <td>
                                <input type="checkbox" id="socio" onChange={this.checkBox} name={'hasPsw'} checked={this.state.hasPsw}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default withRouter(Servicio)
