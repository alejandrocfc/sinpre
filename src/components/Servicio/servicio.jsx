import React, {Component} from "react";
import {Link, withRouter} from "react-router-dom";
import Swal from "sweetalert2";
import { ObrasApi, StocksApi} from "../../helpers/request";
import {fields as Fields} from "../../helpers/constants";
import Card from "../Card";
import Field from "../Fields";
import Titulo from "../Fields/titulo";
import Parrafo from "../Fields/parrafo";
import CampoTexto from "../Fields/campoTexto";
import CampoFecha from "../Fields/campoFecha";
import CampoNumerico from "../Fields/campoNumerico";
import AreaTexto from "../Fields/areaTexto";
import CheckboxList from "../Fields/checkboxList";
import ListaOpciones from "../Fields/listaOpciones";
import ListaCompuesta from "../Fields/listaCompuesta";
import GeoLocalizacion from "../Fields/geoLocalizacion";
import Dimension from "../Fields/dimension";
import Imagenes from "../Fields/imagenes";
import Separator from "../Fields/separator";
import {handleRequestError, uniqueID} from "../../helpers/utils";
import {toast} from "react-toastify";

class FormServicio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields:[],
            stocks:[],
            name:"",
            id:"",
            obra:{}
        };
        this.parentId = "";
    }

    async componentDidMount() {
        //Traer los stocks disponibles}
        if(!this.props.location.state) {
            this.props.history.push({
                pathname: "/no-match",
                state: {message: "Obra no encontrada: "}
            })
        }
        try {
            const obra = this.props.location.state.obra;
            const stocks = await StocksApi.getAll();
            const getForm = await ObrasApi.getForm(obra.id_solicitud);
            if(getForm.status === 1 && getForm.data.id_formulario){
                const {map_formulario:{fields,name}, id_formulario} = getForm.data;
                this.setState({name, fields, id: id_formulario})
            }
            this.setState({obra, stocks: stocks.data})
        }catch (e) {
            console.log(e);
        }
    }

    //Change form name
    changeName = (e) => {
        const {name,value} = e.target;
        this.setState({[name]:value})
    };

    //Drag & Drop from options to list
    onDragStart = (ev, id) => {
        const parentId = ev.target.parentNode.id;
        ev.dataTransfer.effectAllowed = "copy";
        ev.dataTransfer.setData(`${parentId}`, id);
        this.parentId = parentId;
    };

    onDragOver = (ev) => ev.preventDefault();

    onDrop = (ev) => {
        let id = ev.dataTransfer.getData(this.parentId);
        if(!id) return;
        let task;
        if(this.parentId === "campos"){
            task = Fields.find(({type}) => type === id);
            task._id = uniqueID();
            this.setState(prevState => {return {fields: [...prevState.fields, task]}});
        }else{
            let {map_stock} = this.state.stocks.find(({id_stock}) => id_stock === id);
            map_stock.map(stock => stock._id = uniqueID());
            this.setState(prevState => {return {fields: [...prevState.fields, ...map_stock]}});
        }
        this.parentId = ""
    };

    //Drag & Drop list
    onListDrag = (e,index) => {
        this.draggedItem = this.state.fields[index];
        e.dataTransfer.effectAllowed = "move";
        //Necessary for browser handle desired effect
        e.dataTransfer.setData("text/html", e.target.parentNode);
        e.dataTransfer.setDragImage(e.target.parentNode, 20, 20);
    };

    onListOver = (index) => {
        const draggedOverItem = this.state.fields[index];
        // if the item is dragged over itself, ignore
        if (this.draggedItem === draggedOverItem) return;
        if(this.draggedItem) {
            // filter out the currently dragged item
            let items = this.state.fields.filter(item => item !== this.draggedItem);
            // add the dragged item after the dragged over item
            items.splice(index, 0, this.draggedItem);
            this.setState({fields: items});
        }
    };

    onListDragEnd = () => this.draggedItem = null;

    //
    renderContent = (item,idx) => {
        let field = <p>Item</p>;
        switch(item.type) {
            case "title":
                field = <Titulo data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "paragraph":
                field = <Parrafo data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "input":
                field = <CampoTexto data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "date":
                field = <CampoFecha data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "number":
                field = <CampoNumerico data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "textarea":
                field = <AreaTexto data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "list":
                field = <CheckboxList data={item} idx={idx} onChange={this.onChange} addOption={this.addOption} onChangeOption={this.onChangeOption} removeOption={this.removeOption}/>;
                break;
            case "select":
                field = <ListaOpciones data={item} idx={idx} onChange={this.onChange} addOption={this.addOption} onChangeOption={this.onChangeOption} removeOption={this.removeOption}/>;
                break;
            case "nested":
                field = <ListaCompuesta data={item} idx={idx} onChange={this.onChange} stocks={this.state.stocks} renderContent={this.renderContent}/>;
                break;
            case "geo":
                field = <GeoLocalizacion data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "dimension":
                field = <Dimension data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "image":
                field = <Imagenes data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "separator":
                field = <Separator/>;
                break;
            default:
                break;
        }
        return <Field idx={idx} deleteItem={this.deleteItem} editItem={this.editItem}>{field}</Field>;
    };

    onChange = (e,idx) => {
        const fields = this.state.fields.map(u => Object.assign({}, u));
        const {name} = e.target;
        fields[idx][name] = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        this.setState({fields})
    };

    onChangeOption = (e,idx,opt) => {
        const {name, value} = e.target;
        this.setState(state => {
            const list = state.fields.map((item, j) => {
                if (j === idx) {
                    item.options[opt][name] = value;
                    return item;
                } else return item
            });
            return {fields:list};
        });
    };

    addOption = idx => {
        const fields = this.state.fields.map(u=>Object.assign({}, u));
        const current = fields[idx].options.length;
        fields[idx].options.push({label:`Opción ${current+1}`,value:`opcion-${current+1}`});
        this.setState({fields})
    };

    removeOption = (parent,child) => {
        if(child === 0) return;
        this.setState(state => {
            const list = state.fields.map((item, j) => {
                if (j === parent) {
                    item.options = item.options.filter((item,index)=>index !== child);
                    return item;
                } else return item
            });
            return {fields:list};
        });
    };

    deleteItem = idx => {
        this.setState(prevState => (
            {fields:prevState.fields.filter((item,index)=>(index !== idx))}
        ))
    };

    editItem = idx => {
        const fields = this.state.fields.map(u=>Object.assign({}, u));
        fields[idx].edit = !fields[idx].edit;
        this.setState({fields})
    };

    submit = async() => {
        try{
            Swal.fire({
                title: "Espera :)",
                onBeforeOpen: () => Swal.showLoading()
            });
            if(this.state.id){
                const data = {map:{fields:this.state.fields,name:this.state.name},id:this.state.id};
                await ObrasApi.updateForm(this.state.obra.id_solicitud, data);
            }else{
                const data = {fields:this.state.fields,name:this.state.name};
                await ObrasApi.saveForm(this.state.obra.id_solicitud, data);
            }
            Swal.close();
        }catch (error) {
            const reqError = handleRequestError(error);
            toast.error(reqError.status+" "+reqError.message);
        }
    };

    render() {
        const {fields, obra, stocks} = this.state;
        return (
            <section>
                <article className="box">
                    <div className="ph3">
                        <h2>Formulario</h2>
                        <hr className="horizontal"/>
                        <div className="description">
                            <p>Puede crear un nuevo formulario o agregar de la lista de la izquierda bloques ya creados.
                                Los formularios que se agreguen del listado pueden estar compartidos por otros proyectos</p>
                        </div>
                    </div>
                    <div className="buttons">
                        <button className={`button is-success ${this.state.loading?"is-loading":""}`} onClick={this.submit} disabled={fields.length<=0}>Guardar</button>
                        <button className="button  is-small is-text">
                            <Link to={`/obra/${obra.id_solicitud}`}>Cerrar</Link>
                        </button>
                    </div>
                </article>
                <div className="field">
                    <label htmlFor="name" className="label required">Nombre Formulario</label>
                    <input type="text" id="name" name="name" className="input" value={this.state.name} onChange={this.changeName} required/>
                </div>
                <div className="columns">
                    <div className="column is-3 sticky">
                        <Card title={"Bloques Existentes"} color={"info"} scroll={true}>
                            <ul className="control-list pa0" id={"bloques"}>
                                {
                                    stocks.map(stock=>(
                                        <li key={stock.id_stock} onDragStart = {(e) => this.onDragStart(e, stock.id_stock)} draggable className="control-option">
                                            {stock.nombre_stock}
                                        </li>
                                    ))
                                }
                            </ul>
                        </Card>
                    </div>
                    <div className="column">
                        <ul className={`options-list ba ${fields.length<1?"b--dashed":"b--solid"}`} style={{minHeight:"450px"}}
                            onDrop={this.onDrop} onDragOver={this.onDragOver}>
                            {fields.map((item, idx) => (
                                <li key={idx} className="item-list" onDragOver={e=>this.onListOver(idx)}>
                                    <div
                                        className="drag pa2"
                                        draggable
                                        onDragStart = {(e) => this.onListDrag(e, idx)}
                                        onDragEnd={this.onListDragEnd}
                                    >
                                        {this.renderContent(item,idx)}
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                    <div className="column is-2 sticky">
                        <ul className="control-list pa0" id={"campos"}>
                            {Fields.map(t=>{
                                return <li key={t.name}
                                           onDragStart = {(e) => this.onDragStart(e, t.type)}
                                           draggable
                                           className="control-option f4"
                                >
                                    {t.name}
                                </li>
                            })}
                        </ul>
                    </div>
                </div>
            </section>
        )
    }
}

export default withRouter(FormServicio)
