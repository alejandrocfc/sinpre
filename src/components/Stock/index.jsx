import React, {Component, Fragment} from "react";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import Swal from "sweetalert2";
import {StocksApi} from "../../helpers/request";
import {filterList, handleRequestError} from "../../helpers/utils";
import Pagination from "../Pagination";

class Stock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list:[],
            pageOfItems:[]
        }
    }

    componentDidMount() {
        this.fetchStock();
    }

    fetchStock = async() => {
        try {
            const resp = await StocksApi.getAll();
            if(resp.status === 1) this.setState({list: resp.data, original: resp.data});
            else toast.error(resp.msj);
        }catch (error) {
            const reqError = handleRequestError(error);
            toast.error(reqError.status+" "+reqError.message);
        }
    };

    stockDelete = (stock) => {
        this.setState({delete:{id:stock.id_stock,name:stock.nombre_stock}});
        Swal.fire({
            title: "Confirmación",
            type: "question",
            showCancelButton: true,
            html: `Está seguro de eliminar a <b>${stock.nombre_stock}</b>`,
            showLoaderOnConfirm: true,
            preConfirm: async() => {
                try {
                    return await StocksApi.delete(this.state.delete.id);
                }catch (error) {
                    const reqError = handleRequestError(error);
                    Swal.showValidationMessage(reqError.status+" "+reqError.message);
                }
            }
        }).then((result)=>{
            this.setState({delete:{}});
            if(result.value) this.fetchStock();
        })
    };

    searchStock = (e) => {
        const {value} = e.target;
        if (value.length >= 3) {
            let filtered = filterList(this.state.original, 'nombre_stock', value);
            this.setState({list: filtered});
        }else{
            this.setState(prevState => ({list: prevState.original}));
        }
    };

    onChangePage = (pageOfItems) => {
        this.setState({ pageOfItems: pageOfItems });
    };

    render() {
        return (
            <div className="container">
                <div className="columns">
                    <div className="column is-8">
                        <div className="control has-icons-left">
                            <input className="input" type="text" placeholder="Buscar por nombre" onChange={this.searchStock}/>
                            <span className="icon is-small is-left">
                                    <i className="fas fa-search"/>
                                </span>
                        </div>
                    </div>
                    <div className="column is-4">
                        <div className="new" style={{textAlign:'end'}}>
                            <Link to={{pathname:"/stock",state:{edit:false}}}>
                                <button className="button blue">Agregar +</button>
                            </Link>
                        </div>
                    </div>
                </div>
                {
                    this.state.list.length > 0 ?
                        <Fragment>
                            <table className="table is-fullwidth">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th className="has-text-centered">Nombre</th>
                                    <th className="has-text-centered">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    this.state.pageOfItems.map(list=>{
                                        return <tr key={list.id_stock}>
                                            <td>{list.id_stock}</td>
                                            <td>{list.nombre_stock}</td>
                                            <td>
                                                <Link to={{pathname:"/stock",state:{edit:list}}}>
                                                    <button className="button is-text">
                                                        <span className="icon is-small"><i className='fas fa-edit'></i></span>
                                                    </button>
                                                </Link>
                                                <button className="button is-text" onClick={e=>this.stockDelete(list)}>
                                                    <span className="icon is-small"><i className='fas fa-trash-alt'></i></span>
                                                </button>
                                            </td>
                                        </tr>
                                    })
                                }
                                </tbody>
                            </table>
                            <Pagination
                                items={this.state.list}
                                onChangePage={this.onChangePage}/>
                        </Fragment>:
                        <p>No hay stocks aún</p>
                }
            </div>
        )
    }
}

export default Stock
