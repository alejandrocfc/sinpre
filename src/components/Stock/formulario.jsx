import React, {Component} from "react";
//Libraries
import { withRouter, Redirect, Link } from "react-router-dom"
import {toast} from "react-toastify";
//Components and Helpers
import {StocksApi} from "../../helpers/request";
import { fields as Fields } from "../../helpers/constants";
import Card from "../Card";
import Field from "../Fields";
import Titulo from "../Fields/titulo";
import Parrafo from "../Fields/parrafo";
import CampoTexto from "../Fields/campoTexto";
import CampoFecha from "../Fields/campoFecha";
import CampoNumerico from "../Fields/campoNumerico";
import AreaTexto from "../Fields/areaTexto";
import CheckboxList from "../Fields/checkboxList";
import ListaOpciones from "../Fields/listaOpciones";
import ListaCompuesta from "../Fields/listaCompuesta";
import GeoLocalizacion from "../Fields/geoLocalizacion";
import Dimension from "../Fields/dimension";
import Imagenes from "../Fields/imagenes";
import Separator from "../Fields/separator";
import {handleRequestError} from "../../helpers/utils";

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:"",
            name:"",
            fields: [],
            stocks: [],
        };
    }

    async componentDidMount() {
        if(!this.props.location.state) return <Redirect to={"/stocks"}/>;
        try {
            const stocks = await StocksApi.getAll();
            this.setState({stocks: stocks.data});
            const {edit} = this.props.location.state;
            if(edit) this.setState({
                name:edit.nombre_stock,
                fields:edit.map_stock,
                id:edit.id_stock});
        }catch (e) {
            console.log(e);
        }
    }

    changeName = (e) => {
        const {name,value} = e.target;
        this.setState({[name]:value})
    };

    onDragStart = (ev, id) => {
        ev.dataTransfer.effectAllowed = "copy";
        ev.dataTransfer.setData("type", id);
    };

    onDragOver = (ev) => ev.preventDefault();

    onDrop = (ev) => {
        let id = ev.dataTransfer.getData("type");
        if(!id) return;
        const task = JSON.parse(JSON.stringify(Fields.find(task => task.type === id)));
        this.setState(prevState => {
            return {fields: [...prevState.fields, task]}
        });
    };

    onListDrag = (e,index) => {
        this.draggedItem = this.state.fields[index];
        e.dataTransfer.effectAllowed = "move";
        //Necessary for browser handle desired effect
        e.dataTransfer.setData("text/html", e.target.parentNode);
        e.dataTransfer.setDragImage(e.target.parentNode, 20, 20);
    };

    onListOver = (evt,index) => {
        const draggedOverItem = this.state.fields[index];
        // if the item is dragged over itself, ignore
        if (this.draggedItem === draggedOverItem) return;
        if(this.draggedItem) {
            // filter out the currently dragged item
            let items = this.state.fields.filter(item => item !== this.draggedItem);
            // add the dragged item after the dragged over item
            items.splice(index, 0, this.draggedItem);
            this.setState({fields: items});
        }
    };

    onListDragEnd = () => this.draggedItem = null;

    renderContent = (item,idx) => {
        let field = <p>Item</p>;
        switch(item.type) {
            case "title":
                field = <Titulo data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "paragraph":
                field = <Parrafo data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "input":
                field = <CampoTexto data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "date":
                field = <CampoFecha data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "number":
                field = <CampoNumerico data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "textarea":
                field = <AreaTexto data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "list":
                field = <CheckboxList data={item} idx={idx} onChange={this.onChange} addOption={this.addOption} onChangeOption={this.onChangeOption} removeOption={this.removeOption}/>;
                break;
            case "select":
                field = <ListaOpciones data={item} idx={idx} onChange={this.onChange} addOption={this.addOption} onChangeOption={this.onChangeOption} removeOption={this.removeOption}/>;
                break;
            case "nested":
                field = <ListaCompuesta data={item} idx={idx} onChange={this.onChange} stocks={this.state.stocks} renderContent={this.renderContent}/>;
                break;
            case "geo":
                field = <GeoLocalizacion data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "dimension":
                field = <Dimension data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "image":
                field = <Imagenes data={item} idx={idx} onChange={this.onChange}/>;
                break;
            case "separator":
                field = <Separator/>;
                break;
            default:
                break;
        }
        return <Field idx={idx} deleteItem={this.deleteItem} editItem={this.editItem}>{field}</Field>;
    };

    onChange = (e,idx) => {
        const fields = this.state.fields.map(u => Object.assign({}, u));
        const {name} = e.target;
        fields[idx][name] = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        this.setState({fields})
    };

    onChangeOption = (e,idx,opt) => {
        const {name, value} = e.target;
        this.setState(state => {
            const list = state.fields.map((item, j) => {
                if (j === idx) {
                    item.options.map((optn,key) => {
                        if(key === opt){
                            optn[name] = value;
                            return optn;
                        }else return optn;
                    });
                    return item
                } else return item
            });
            return {fields:list};
        });
    };

    addOption = idx => {
        this.setState(state => {
            const list = state.fields.map((item, j) => {
                if (j === idx) {
                    const current = item.options.length;
                    item.options.push({label:`Opción ${current+1}`,value:`opcion-${current+1}`});
                    return item;
                } else {
                    return item;
                }
            });
            return {
                fields:list,
            };
        });
    };

    removeOption = (parent,child) => {
        if(child === 0) return;
        this.setState(state => {
            const list = state.fields.map((item, j) => {
                if (j === parent) {
                    item.options = item.options.filter((item,index)=>index !== child);
                    return item;
                } else return item
            });
            return {fields:list};
        });
    };

    deleteItem = idx => {
        this.setState(prevState => (
            {fields:prevState.fields.filter((item,index)=>(index !== idx))}
        ))
    };

    editItem = idx => {
        const fields = this.state.fields.map(u=>Object.assign({}, u));
        fields[idx].edit = !fields[idx].edit;
        this.setState({fields})
    };

    submit = async(e) =>  {
        this.setState({loading:true});
        try {
            if(this.state.id){
                const data = {id: this.state.id, name : this.state.name, fields : this.state.fields};
                await StocksApi.update(data);
                toast.info('Actualizado');
            }else{
                const data = {name : this.state.name, fields : this.state.fields};
                await StocksApi.create(data);
                toast.success('Creado');
                this.props.history.push("/")
            }
        }catch (error){
            const reqError = handleRequestError(error);
            toast.error(reqError.status+" "+reqError.message);
        }
    };

    render() {
        if(!this.props.location.state) return <Redirect to={"/stocks"}/>;
        const {edit} = this.props.location.state;
        const {fields,name} = this.state;
        return (
            <Card title={edit?"Editar":"Nuevo"} color={"info"}>
                <div className="field">
                    <label htmlFor="name" className="label required">Nombre</label>
                    <input className="input" type="text" id="name" name="name" value={name} onChange={this.changeName} required/>
                </div>
                <hr className="horizontal"/>
                <div className="columns">
                    <div className="column is-2" style={{position:"sticky"}}>
                        <div className="buttons">
                            <button className={`button is-success ${this.state.loading?"is-loading":""}`} onClick={this.submit} disabled={fields.length<=0}>Guardar</button>
                            <button className="button  is-small is-text">
                                <Link to={"/"}>Cerrar</Link>
                            </button>
                        </div>
                        <ul className="control-list">
                            {Fields.map(t=>{
                                return <li key={t.name}
                                     onDragStart = {(e) => this.onDragStart(e, t.type)}
                                     draggable
                                     className="control-option"
                                >
                                    {t.name}
                                </li>
                            })}
                        </ul>
                    </div>
                    <div className="column">
                        <ul className={`options-list ba ${fields.length<1?"b--dashed":"b--solid"}`} onDrop={this.onDrop} onDragOver={this.onDragOver}>
                            {
                                fields.length<1 && <p className="empty-list">Arrastra un campo para empezar</p>
                            }
                            {fields.map((item, idx) => (
                                <li key={idx} className="item-list" onDragOver={e=>this.onListOver(e,idx)}>
                                    <div
                                        className="drag pa2"
                                        draggable
                                        onDragStart = {(e) => this.onListDrag(e, idx)}
                                        onDragEnd={this.onListDragEnd}
                                    >
                                        {this.renderContent(item,idx)}
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </Card>
        )
    }
}

export default withRouter(Item)
