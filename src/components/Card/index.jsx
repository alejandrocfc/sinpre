import React from "react";

export default function Card({...props}) {
    return (
        <div className="card">
            <div className="card-header">
                {
                    props.title &&
                    <div className={`card-header-title card-header-item ${props.color+'CardHeader'}`}><p className='has-text-white'>{props.title}</p></div>
                }
            </div>
            <div className={`card-content ${props.scroll?"scrollable":""}`}>
                {props.children}
            </div>
        </div>
    )
}
