import axios from "axios";
import {cryptSha1} from "./utils";
import {keyApi,loginApi} from "./constants";

const Axios = axios.create({
    baseURL: "https://www.ingelecgroup.com/sinpre/ApiRest"
});

let date = new Date();
if(date.getHours()>18) date = new Date(date.setDate(date.getDate()+1));
const parsed = date.getFullYear() + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + ('0' + date.getDate()).slice(-2);

///getObra?obra=5600

const Actions = {
    get: async (url) => {
        try{
            return await Axios.get(url).then(({data})=>data);
        }
        catch (e) {return e}
    },
    post: async (url, data) => {
        try{
            return await Axios.post(url,data).then(({data})=>data);
        }
        catch (e) {return e}
    },
    delete: async (url, id) => {
        try{
            return await Axios.delete(`${url}/${id}`).then(({data})=>data);
        }
        catch (e) {return e}
    },
    put: async (url, data) => {
        try{
            return await Axios.put(url,data).then(({data})=>data);
        }
        catch (e) {return e}
    }
};

export const StocksApi = {
    get: async (id) => {
        return await Actions.get(`getStock?idStock=${id}`);
    },
    create: async (data) => {
        const body = {
            key:cryptSha1("*"+keyApi+""+parsed+"*"),
            login:cryptSha1(cryptSha1(loginApi)),
            map:data.fields,
            nombre:data.name
        };
        return await Actions.post("addStock", body);
    },
    update: async (data) => {
        const body = {
            key:cryptSha1("*"+keyApi+""+parsed+"*"),
            login:cryptSha1(cryptSha1(loginApi)),
            id:data.id,
            map:data.fields,
            nombre:data.name
        };
        return await Actions.put(`updateStock`, body);
    },
    delete: async (id) => {
        const body = {
            key:cryptSha1("*"+keyApi+""+parsed+"*"),
            login:cryptSha1(cryptSha1(loginApi)),
            id:id
        };
        return await Axios.post(`deleteStock`, body);
    },
    getAll: async () => {
        return await Actions.get("getStockFormulario");
    }
};

export const ObrasApi = {
    getInfo: async (id) => {
        return await Actions.get(`getObra?obra=${id}`)
    },
    saveForm: async (id, data) => {
        const body = {
            key:cryptSha1("*"+keyApi+""+parsed+"*"),
            login:cryptSha1(cryptSha1(loginApi)),
            map:data,
            solicitud:id
        };
        return await Actions.post(`addFormulario`,body)
    },
    updateForm: async (id, data) => {
        const body = {
            key:cryptSha1("*"+keyApi+""+parsed+"*"),
            login:cryptSha1(cryptSha1(loginApi)),
            id:data.id,
            map:data.map,
            solicitud:id
        };
        return await Actions.put(`updateFormulario`,body)
    },
    getForm: async (id) => {
        return await Actions.get(`getFormulario?obra=${id}`)
    }
};
