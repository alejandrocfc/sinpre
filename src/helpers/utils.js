import CryptoJS from "crypto-js"

export function handleRequestError(error) {
    const info = {};
    if (error.response) {
        info.status = error.response.status;
        info.message = error.response.data.message ? error.response.data.message : JSON.stringify(error.response.data);
    } else if (error.request) {
        console.log(error.request);
        info.status = 700;
        info.message = 'Network Error';
    } else {
        info.status = 800;
        info.message = error.message;
    }
    console.log(error);
    return info;
}


export function uniqueID(){
    return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5))
}

export function renderSimpleItem(item) {
    return `<${item.subtype}> ${item.label?item.label:item.name} </${item.subtype}>`;
}

export function renderInput(item) {
    let field = `<div class="control">`;
    field += item.required ? `<label for="name" class="label required">${item.label ? item.label : item.name}</label>` : `<label for="name" class="label">${item.label?item.label:item.name}</label>`;
    field += `<input type=${item.subtype} class="input" `;
    if(item.placeholder)
        field += `placeholder="${item.placeholder}" `;
    if(item.maxlength)
        field += `maxlength="${item.maxlength}" `;
    field += ` id="name" name="name"/></div>`;
    return field;
}

export function renderNumber(item) {
    let field = `<div class="control">`;
    field += item.required ? `<label for="name" class="label required">${item.label ? item.label : item.name}</label>` : `<label for="name" class="label">${item.label?item.label:item.name}</label>`;
    if(item.decimals)
        field += `<input class="input" type=${item.subtype} min="${item.min}" max="${item.max}" step="0.1" id="name" name="name"/></fieldset>`;
    else
        field += `<input class="input" type=${item.subtype} min="${item.min}" max="${item.max}" id="name" name="name"/></div>`;
    return field;
}

export function renderTextArea(item) {
    let field = `<div class="control">`;
    field += item.required ? `<label for="name" class="label required">${item.label ? item.label : item.name}</label>` : `<label for="name" class="label">${item.label ? item.label : item.name}</label>`;
    field += `<textarea class="textarea" maxlength="${item.maxlength}" rows="${item.rows}" `;
    if(item.placeholder)
        field += `placeholder="${item.placeholder}" `;
    field += ` id="name" name="name"></textarea></div>`;
    return field;
}

export function renderSelect(item) {
    let field = `<div class="control">`;
    field += item.required ? `<label for="name" class="label required">${item.label ? item.label : item.name}</label>` : `<label for="name" class="label">${item.label ? item.label : item.name}</label>`;
    field += `<select class="select is-fullwidth" id="name" name="name">`;
    item.options.map(option=>(
        field += `<option value="${option.value}">${option.label}</option>`
    ));
    field += `</select></div>`;
    return field;
}

export function renderChecboxList(item) {
    let field = `<div class="control">`;
    field += item.required ? `<label for="name" class="label required">${item.label ? item.label : item.name}</label>` : `<label for="name" class="label">${item.label ? item.label : item.name}</label>`;
    item.options.map(option=>(
        field += `<div><input name="${option.label}" type="checkbox"></input><label for="${option.label}">${option.label}</label></div>`
    ));
    field += `</div>`;
    return field;
}

export function cryptSha1(value) {
    return CryptoJS.SHA1(value).toString()
}

export function filterList(list, prop, value) {
    return list.filter(item => item[prop].search(new RegExp(value, 'i')) >= 0)
}
