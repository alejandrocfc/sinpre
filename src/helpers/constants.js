export const fields = [
    {"type":"title","subtype":"h2","name":"Título","label":"","edit":false},
    {"type":"paragraph","subtype":"p","name":"Párrafo","label":"","edit":false},
    {"type":"input","subtype":"text","name":"Campo Texto","label":"","required":false,"placeholder":"","edit":false,"maxlength":10},
    {"type":"date","subtype":"date","name":"Campo Fecha","label":"","required":false,"edit":false},
    {"type":"number","subtype":"number","name":"Campo Numérico","label":"","required":false,"min":0,"max":0,"decimals":false,"edit":false},
    {"type":"textarea","subtype":"textarea","name":"Area de Texto","label":"","edit":false,"required":false,"placeholder":"","maxlength":120},
    {"type":"list","subtype":"checkbox","name":"Grupo de Casillas","label":"","edit":false,"required":false,"options":[{"label":"Opción 1","value":"opcion-1"}]},
    {"type":"select","subtype":"select","name":"Lista de Opciones","label":"","edit":false,"required":false,"options":[{"label":"Opción 1","value":"opcion-1"}]},
    {"type":"nested","name":"Lista con Campos","label":"","placeholder":"","form":"","number":1,"required":false,"variable":false},
    {"type":"geo","name":"Geolocalización","label":"","placeholder":"","required":false},
    {"type":"dimension","name":"Dimensión","label":"","placeholder":"","required":false},
    {"type":"image","name":"Imágenes","label":"","placeholder":"","multi":false,"required":false},
    {"type":"separator","name":"Separador","label":"","edit":false}
];

export const keyApi = "asdfghjklerty";

export const loginApi = "1NG3L3C2020@group";
