## Ingelec Group

### Descripción
Proyecto para administrar los formularios de los servicios.

### Tecnologías
- ReactJS

### Instalación
1. Clonar repositorio https://gitlab.com/alejandrocfc/sinpre
2. Dentro de la carpeta instalar dependencias ``npm install``
3. Comandos de ejecución
    - ``npm run start`` Ejecuta entorno local
    - ``npm run build`` Prepara carpeta para despliegue

### Producción
Actualmente el proyecto está alojado en *Netlify* con integración continua sobre la rama master. Así que cada push sobre esta rama despliega la aplicación.  
La aplicación puede ser vista en https://sinpre.netlify.com/
